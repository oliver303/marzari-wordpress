<footer>
    <p class="pull-right"><a href="#">Back to top</a></p>
    <p><?php bloginfo('description');?> <a href="#">Privacy</a> &middot; <a href="#">Terms</a></p>
</footer>

</div>

<!-- Bootstrap core JavaScript -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php bloginfo('template_url');?>/bootstrap/js/bootstrap.min.js"></script>
<script>
    $(document).ready(function() {
        // Alle Submit-Buttons mit Bootstrap formatieren
       $("#submit").addClass('btn btn-default btn-large');
    });
</script>

<?php wp_footer(); ?>
</body>
</html>