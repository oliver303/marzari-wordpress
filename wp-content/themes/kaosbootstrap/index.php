<?php get_header(); ?>

    <div class="container content">

<?php the_post(); ?>

    <div class="row">
        <div class="col-lg-12">
            <?php the_content(); ?>
        </div>
    </div>

<?php get_footer(); ?>