<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link href="<?php bloginfo('template_url');?>/style.css" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="<?php bloginfo('template_url');?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url');?>/bootstrap/css/carousel.css" rel="stylesheet">
    <?php wp_head(); ?>
</head>

<body>


<div class="container-fluid header-wrapper">
    <div class="container-fluid left">
    <div class="container-fluid right">
    </div>
    </div>
</div>
<div class="container header">
    <div class="row">
        <div class="col-md-3 header-left">
            <div class="logo">
                <a href="/" title="Zur Startseite"><img src="<?php bloginfo('template_url');?>/images/logo_marzari_technik.png" alt="Logo Marzari Technik GmbH" /></a>
            </div>
        </div>
        <div class="col-md-9 header-right">
            <div class="top1">
                    <?php if ( is_active_sidebar( 'suche' ) ) : ?>
                        <div class="kontaktdaten1">
                            <?php dynamic_sidebar( 'suche' ); ?>
                        </div>
                    <?php endif; ?>
            </div>
            <div class="top2">Test Top 2</div>
        </div>
    </div>
</div>








<!--
<div class="navbar-wrapper">
    <div class="container">

        <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="<?php bloginfo('url');?>"><?php bloginfo('name');?></a>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <?php
                    $defaults = array(
                        'theme_location' => 'primary',
                        'container' => 'false',
                        'menu_class' => 'nav navbar-nav',
                        'depth' => 2,
                        'walker' => new wp_bootstrap_navwalker(),
                    );
                    wp_nav_menu($defaults)
                    ;?>
                </div>
            </div>
        </nav>
        
    </div>
</div>
-->